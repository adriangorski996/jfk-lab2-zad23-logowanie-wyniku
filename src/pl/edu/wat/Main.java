package pl.edu.wat;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.ast.nodeTypes.NodeWithBody;
import com.github.javaparser.ast.nodeTypes.NodeWithStatements;
import com.github.javaparser.ast.stmt.BlockStmt;
import com.github.javaparser.ast.stmt.IfStmt;
import com.github.javaparser.ast.stmt.ReturnStmt;

import javax.tools.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class Main {

    public static void main(String[] args) throws IOException {
        final String fileName = "src\\Class.java";
        final String alteredFileName = "src\\ClassAltered.java";
        CompilationUnit cu;
        try (FileInputStream in = new FileInputStream(fileName)) {
            cu = JavaParser.parse(in);
        }

        cu.getChildNodesByType(MethodDeclaration.class).forEach(Main::weaveLog);
        if (cu.getClassByName("Class").isPresent()) {
            cu.getClassByName("Class").get().setName("ClassAltered");
        }

        try (FileWriter output = new FileWriter(new File(alteredFileName), false)) {
            output.write(cu.toString());
        }

        File[] files = {new File(alteredFileName)};
        String[] options = {"-d", "out//production//Synthesis"};

        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<>();
        try (StandardJavaFileManager fileManager = compiler.getStandardFileManager(null, null, null)) {
            Iterable<? extends JavaFileObject> compilationUnits =
                    fileManager.getJavaFileObjectsFromFiles(Arrays.asList(files));
            compiler.getTask(
                    null,
                    fileManager,
                    diagnostics,
                    Arrays.asList(options),
                    null,
                    compilationUnits).call();

            diagnostics.getDiagnostics().forEach(d -> System.out.println(d.getMessage(null)));
        }
    }

    private static BlockStmt GetMethodStmt(MethodDeclaration method) {
        BlockStmt block;
        Optional<BlockStmt> body = method.getBody();
        if (!body.isPresent()) {
            block = new BlockStmt();
            method.setBody(block);
        } else {
            block = body.get();
        }
        return block;
    }

    private static void weaveLog(MethodDeclaration method) {
        BlockStmt block = GetMethodStmt(method);
        List<ReturnStmt> returnStmts = block.getChildNodesByType(ReturnStmt.class);
        returnStmts.forEach(r -> {
            if (r.getParentNode().isPresent()) {
                if (r.getParentNode().get() instanceof NodeWithStatements) {
                    MethodCallExpr call = logMethod(r, method);
                    Node tmp = r.getParentNode().get();
                    tmp.remove(r);
                    ((NodeWithStatements) tmp).addStatement(call);
                    ((NodeWithStatements) tmp).addStatement(r);
                } else if (r.getParentNode().get() instanceof NodeWithBody) {
                    MethodCallExpr call = logMethod(r, method);
                    Node tmp = r.getParentNode().get();
                    tmp.remove(r);
                    BlockStmt blockStmt = ((NodeWithBody) tmp).createBlockStatementAsBody();
                    blockStmt.addStatement(call);
                    blockStmt.addStatement(r);
                } else if (r.getParentNode().get() instanceof IfStmt) {
                    MethodCallExpr call = logMethod(r, method);
                    IfStmt tmp = (IfStmt) r.getParentNode().get();
                    BlockStmt blockStmt = new BlockStmt();
                    blockStmt.addStatement(call);
                    blockStmt.addStatement(r);
                    tmp.replace(r, blockStmt);
                }
            }
        });
    }


    private static MethodCallExpr logMethod(ReturnStmt r, MethodDeclaration method) {

        MethodCallExpr call = new MethodCallExpr(null, "Logger.log");
        call.addArgument("\"" + method.getNameAsString() + "\"");
        if (r.getExpression().isPresent()) {
            call.addArgument(r.getExpression().get());
        } else {
            call.addArgument("");
        }
        return call;
    }
}
