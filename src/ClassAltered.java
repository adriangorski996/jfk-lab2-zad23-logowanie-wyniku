import static java.lang.System.out;

public final class ClassAltered {

    public static void main(String[] args) {
        String upper = upper("Adrian G�rski");
        out.println(upper + " = " + count(upper));
    }

    private static String upper(String s) {
        String aux = s.toUpperCase();
        if (s.contains("a")) {
            int a = 0;
            Logger.log("upper", aux);
            return aux;
        } else {
            int a = 0;
            Logger.log("upper", "string");
            return "string";
        }
    }

    public static int count(String s) {
        int length = s.length();
        Logger.log("count", length);
        return length;
    }

    public static String letsTrySwitch(int i) {
        switch(i) {
            case 0:
                int a = 0;
                Logger.log("letsTrySwitch", "zero");
                return "zero";
            case 1:
                Logger.log("letsTrySwitch", "one");
                return "one";
            case 2:
                Logger.log("letsTrySwitch", "two");
                return "two";
            case 3:
                Logger.log("letsTrySwitch", "three");
                return "three";
            case 4:
                Logger.log("letsTrySwitch", "four");
                return "four";
            case 5:
                Logger.log("letsTrySwitch", "five");
                return "five";
            case 6:
                Logger.log("letsTrySwitch", "six");
                return "six";
            default:
                Logger.log("letsTrySwitch", "i am baby method and don't know another numbers :(");
                return "i am baby method and don't know another numbers :(";
        }
    }

    public static String ifek(int i) {
        if (i == 1) {
            Logger.log("ifek", "return in then");
            return "return in then";
        } else {
            Logger.log("ifek", "return in else");
            return "return in else";
        }
    }

    public static String dowhiler() {
        do {
            Logger.log("dowhiler", "dowhiler");
            return "dowhiler";
        } while (true);
    }

    public static String whiler() {
        while (true) {
            Logger.log("whiler", "whiler");
            return "whiler";
        }
    }

    public static String forer() {
        for (int i = 0; i < 3; i++) {
            Logger.log("forer", "forer");
            return "forer";
        }
        Logger.log("forer", "forer outside for");
        return "forer outside for";
    }
}
