import static java.lang.System.out;

class Logger {

    public static void log(String name, Object ret){
        out.println(name + " returned " + ret.toString());
    }
}